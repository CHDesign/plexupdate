#!/bin/bash
echo "Getting URL for plex download"
echo "Please enter in URL for plex download"

#OLD
#read link

#Call to python3 script to get link from plex website
link=$(ssh python-app /opt/rh/rh-python36/root/bin/python3 /home/plex/plex-update.py)

echo "link = $link"

ssh plex service plexmediaserver stop
echo "Downloading RPM"
ssh plex wget -q -P /var/tmp/ $link
RPM=$(ssh plex ls /var/tmp/ | grep .rpm | head -1)

#DEBUG
#echo "RPM = $RPM"

ssh plex rpm -Uvh /var/tmp/$RPM
echo "RPM installed: Restarting Plex on Plex"

ssh -t plex service plexmediaserver start 2> /dev/null
ssh plex rm /var/tmp/$RPM
ssh -t plex service plexmediaserver status
echo "Update Done on Plex"
