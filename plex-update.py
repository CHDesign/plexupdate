from selenium import webdriver
import pandas as pd
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from bs4 import BeautifulSoup
import time

#import pdb; pdb.set_trace()


caps = DesiredCapabilities().CHROME
caps["pageLoadStrategy"] = "eager"  #  complete
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--window-size=800,600')
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
prefs = {'profile.managed_default_content_settings.images':2}
chrome_options.add_experimental_option("prefs", prefs)

driver = webdriver.Chrome(desired_capabilities=caps, chrome_options=chrome_options)
try :
    driver.get("https://www.plex.tv/media-server-downloads/")
    soup = BeautifulSoup(driver.page_source, 'html.parser')

    dropdown = Select(driver.find_element_by_css_selector('.plex-downloads-pms-platform'))
    dropdown.select_by_value('Linux')
    time.sleep(3)

    soup = BeautifulSoup(driver.page_source, 'html.parser')
    version_full = soup.find("p", {"id": "version"})
    version = version_full.get_text()
  
    base_link = "https://downloads.plex.tv/plex-media-server-new/"
    link_full = base_link + version
    link_full = link_full + "/redhat/plexmediaserver-"
    link_full = link_full + version
    link_full = link_full + ".x86_64.rpm"
    print(link_full)

    #f = open("version_plex.txt", "w")
    #f.write(link_full)
    #f.close()
    driver.quit()

except TimeoutException as e:
    print("Page load Timeout Occured. Quiting !!!")
    driver.quit()

driver.quit()
